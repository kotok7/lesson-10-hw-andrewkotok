package HowToString.TastksFrom2toll5;

public class Methods {
//task 2
    public int indSymbolOccurance (String s,char c){
        int count = 0;
        char[] f = s.toCharArray();
        for (char value : f) {
            if (value == c) {
                count++;
            }
        }
        return count;
    }
public int findWordPosition (String source, String target){
    if ((source.length() >= target.length()) && (source.contains(target))){
        return source.indexOf(target);
    } else return -1;
    }

    public String stringReverse (String s){
        StringBuilder result = new StringBuilder(s.length());
        for (int i = (s.length() - 1); i >= 0; i--){
            result.append(s.charAt(i));
        }
        return result.toString();
    }

    public boolean isPalindrome (String s){
        return s.equals(stringReverse(s));
    }

}
