package HowToString.Tastk6;

import java.util.Scanner;

public class GuessMethods {
    private final Scanner scanner = new Scanner(System.in);
    private final StringBuilder sharp = new StringBuilder("###############");
    private boolean flag = true;

   public void worldResult(String comp) {
       while (flag) {
           String player = scanner.nextLine();
           if (!player.equals(comp)) {
               for (int i = 0; i < player.toCharArray().length; i++) {
                   if (comp.charAt(i) == player.charAt(i)) {
                       sharp.setCharAt(i, comp.charAt(i));
                   } else {
                       sharp.setCharAt(i, '#');
                   }
               }
               System.out.println(sharp);
           } else {
               System.out.println("===========");
               System.out.println("YOU WIN !!!");
               System.out.println("===========");
               flag = false;
           }
       }
   }

}
