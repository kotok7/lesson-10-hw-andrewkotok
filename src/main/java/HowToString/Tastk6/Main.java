package HowToString.Tastk6;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli",
                "carrot", "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom",
                "nut", "olive", "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};
        WordsMethods message = new WordsMethods();
        message.welcomeMessage();
        Random random = new Random();
        String computerGuessTheWord = words[random.nextInt(words.length)];
        System.out.println(computerGuessTheWord);
        message.guessMessage();
        GuessMethods run = new GuessMethods();
        run.worldResult(computerGuessTheWord);
    }
}
